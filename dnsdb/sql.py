import os
import logging
import s3
import boto3

from dnsdb import resources_path
from resources.lambda_dnsdb_query import Athena  # So that we don't have to duplicate this code

logging.basicConfig(level=logging.INFO)


def create_dns_queries_table(bucket, data_key, db_name):
    temp_athena_output_key = '{}/_temp_athena_output'.format(data_key)
    temp_athena_output_full_path = 's3://{}/{}'.format(bucket, temp_athena_output_key)
    data_path = 's3://{}/{}/'.format(bucket, data_key)

    # athena = Athena(database='default', output_location=temp_athena_output_full_path)
    athena = Athena(database=db_name, output_location=temp_athena_output_full_path)
    athena.execute_and_wait(query_string='CREATE SCHEMA IF NOT EXISTS {}'.format(db_name))

    with open(os.path.join(resources_path, 'template_create_table_dns_queries.sql')) as query_file:
        query_str = query_file.read().format(location=data_path)
        athena.execute_and_wait(query_string=query_str)

    # Remove temporary files
    s3.recursive_delete(bucket=bucket, key_prefix=temp_athena_output_key)


def drop_db(db_name):
    glue = boto3.client('glue')
    try:
        glue.delete_database(Name=db_name.lower())
    except glue.exceptions.EntityNotFoundException as e:
        logging.info("Database %s does not exist. Doing nothing", db_name)
