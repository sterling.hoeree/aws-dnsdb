import os
import boto3
import logging
import yaml
import json
from StringIO import StringIO

from dnsdb import resources_path

logging.basicConfig(level=logging.INFO)


def create_or_get_api(unique_id, lambda_arn, lambda_api_version='2015-03-31'):
    gateway_client = boto3.client('apigateway')
    gateway_region = gateway_client.meta.region_name

    # If the API Gateway REST API already exists, return it
    existing_apis = gateway_client.get_rest_apis()
    existing_api_id = filter(lambda x: x['name'] == 'DNS DB {}'.format(unique_id), existing_apis['items'])
    if existing_api_id:
        logging.info("Rest API (%s) 'DNS DB %s' already exists", existing_api_id[0]['id'], unique_id)
        return gateway_client.get_rest_api(restApiId=existing_api_id[0]['id'])

    # Create the base API from the yaml file, which doesn't include method integrations with Lambda
    yaml_buf = StringIO()
    with open(os.path.join(resources_path, 'swagger_rest_api.yaml')) as f:
        yaml_buf.write(f.read() % {'unique_id': unique_id})

    json_buf = StringIO()
    yaml_buf.seek(0)
    json.dump(yaml.load(yaml_buf), fp=json_buf)

    json_buf.seek(0)
    rest_api_info = gateway_client.import_rest_api(failOnWarnings=True, body=json_buf)

    api_id = rest_api_info['id']
    api_name = rest_api_info['name']
    logging.info("Created API Gateway REST API: (%s) %s", api_id, api_name)

    # Find the ids of the resources
    resources = gateway_client.get_resources(restApiId=api_id)
    resources = filter(lambda x: 'resourceMethods' in x, resources['items'])
    ips_for_domain = filter(lambda x: x['path'] == u'/ips_for_domain/{domain}', resources)[0]
    domains_for_ip = filter(lambda x: x['path'] == u'/domains_for_ip/{ip}', resources)[0]

    # Put the AWS integration linking to the Lambda function
    lambda_uri = 'arn:aws:apigateway:{aws_region}:lambda:path/{api_version}/functions/{lambda_arn}' \
                 '/invocations'.format(aws_region=gateway_region, api_version=lambda_api_version, lambda_arn=lambda_arn)
    logging.info("Using this Lambda URI for integration: %s", lambda_uri)
    for resource in [ips_for_domain, domains_for_ip]:
        logging.info("Linking (%s) %s <- (%s) %s", api_id, api_name, resource['id'], resource['path'])
        gateway_client.put_integration(
            restApiId=api_id,
            resourceId=resource['id'],
            httpMethod='GET',
            integrationHttpMethod='POST',
            type='AWS_PROXY',
            uri=lambda_uri,
            passthroughBehavior='WHEN_NO_MATCH',
        )

    return rest_api_info


def deploy_api(gateway_id, stage_name):
    gateway_client = boto3.client('apigateway')
    return gateway_client.create_deployment(restApiId=gateway_id, stageName=stage_name)


def delete_api(unique_id):
    api_name = 'DNS DB {}'.format(unique_id)
    gateway_client = boto3.client('apigateway')
    ids = filter(lambda x: x['name'] == api_name, gateway_client.get_rest_apis()['items'])
    if ids:
        api_id = str(ids[0]['id'])
        logging.info("Deleting REST API (%s) %s", api_id, api_name)
        gateway_client.delete_rest_api(restApiId=api_id)
