import os
import boto3
import yaml
import json
import logging

from dnsdb import resources_path


logging.basicConfig(level=logging.INFO)


def create_or_get_role(role_name, policy_file_name='lambda_basic_policy.yaml', attach_managed_roles=None):
    attach_managed_roles = attach_managed_roles or []

    with open(os.path.join(resources_path, policy_file_name)) as f:
        role_policy_document = yaml.load(f)

    iam_client = boto3.client('iam')

    try:
        iam_client.get_role(RoleName=role_name)
        logging.info('Role %s already exists', role_name)
    except iam_client.exceptions.NoSuchEntityException as e:
        logging.info('Role %s does not exist; creating', role_name)
        iam_client.create_role(
            RoleName=role_name,
            AssumeRolePolicyDocument=json.dumps(role_policy_document),
        )

    for r in attach_managed_roles:
        logging.info('Attaching %s to IAM role %s', r, role_name)
        attach_aws_managed_policy_to_role(role_name=role_name, managed_aws_policy_name=r)

    return iam_client.get_role(RoleName=role_name)


def attach_aws_managed_policy_to_role(role_name, managed_aws_policy_name):
    """
    Attach a built-in AWS managed policy to a role.
    This function assumes that the managed_aws_policy_name's ARN looks like e.g.:
    arn:aws:iam::aws:policy/AmazonS3FullAccess, where AmazonS3FullAccess is the managed_aws_policy_name.

    :param role_name: The name of the role to attach the policy to
    :param managed_aws_policy_name: The name of the managed AWS built-in policy to attach
    :return: The result of attaching the IAM policy as a dict
    """

    iam_client = boto3.client('iam')
    return iam_client.attach_role_policy(RoleName=role_name,
                                         PolicyArn='arn:aws:iam::aws:policy/{}'.format(managed_aws_policy_name))


def delete_role(role_name):
    iam_client = boto3.client('iam')

    try:
        attached_role_policies = iam_client.list_attached_role_policies(RoleName=role_name)
        logging.info("Managed policies attached in role %s are: %s", role_name, str(attached_role_policies))
        if attached_role_policies:
            policy_names = attached_role_policies['AttachedPolicies']
            for policy in policy_names:
                logging.info("Detaching %s from %s", policy['PolicyArn'], role_name)
                iam_client.detach_role_policy(RoleName=role_name, PolicyArn=str(policy['PolicyArn']))

        role_policies = iam_client.list_role_policies(RoleName=role_name)
        logging.info("Unmanaged policies in role %s are: %s", role_name, str(role_policies))
        if role_policies:
            policy_names = role_policies['PolicyNames']
            for policy in policy_names:
                logging.info("Deleting %s from %s", policy, role_name)
                iam_client.delete_role_policy(RoleName=role_name, PolicyName=str(policy))

        iam_client.delete_role(RoleName=role_name)

    except iam_client.exceptions.NoSuchEntityException as e:
        logging.info("Role %s does not exist. Doing nothing", role_name)
