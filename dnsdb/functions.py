import os
import boto3
import logging
import zipfile
import time
from StringIO import StringIO

from dnsdb import resources_path


logging.basicConfig(level=logging.INFO)


def create_or_update_dnsdb_query_lambda(unique_id, bucket_name, db_name, lambda_role_arn, lambda_timeout_s=10):
    function_name = 'lambda_dnsdb_query_py_{}'.format(unique_id)
    settings_dict = {
        'S3_BUCKET': bucket_name,
        'S3_ATHENA_OUTPUT_KEY_PREFIX': '{}/athena-output'.format(unique_id),
        'ATHENA_DATABASE': db_name,
        'ATHENA_TABLE': 'dns_queries',
    }

    script_path = os.path.join(resources_path, 'lambda_dnsdb_query.py')
    zipped_script_bytes = StringIO()
    with zipfile.ZipFile(zipped_script_bytes, 'w') as zf:
        zf.write(script_path, os.path.basename(script_path))
    zipped_script_bytes.seek(0)

    awslambda = boto3.client('lambda')

    try:
        awslambda.get_function(FunctionName=function_name)

        logging.info("Lambda function %s already exists; updating code", function_name)
        return awslambda.update_function_code(
            FunctionName=function_name,
            ZipFile=zipped_script_bytes.read(),
        )
    except awslambda.exceptions.ResourceNotFoundException as e:
        logging.info("Creating lambda function %s with environment: %s", function_name, str(settings_dict))
        logging.info("Lambda %s is assuming role: '%s'", function_name, lambda_role_arn)

        return awslambda.create_function(
            FunctionName=function_name,
            Runtime='python2.7',
            Role=lambda_role_arn,
            Handler='lambda_dnsdb_query.handler',
            Code={
                'ZipFile': zipped_script_bytes.read()
            },
            Timeout=lambda_timeout_s,
            Environment={
                'Variables': settings_dict
            },
        )


def add_gateway_access_to_lambda(lambda_name, gateway_api_id):
    awslambda = boto3.client('lambda')
    region = awslambda.meta.region_name
    account_id = boto3.client('sts').get_caller_identity().get('Account')

    # for resource in ['ips_for_domain', 'domains_for_ip']:
    source_arn = 'arn:aws:execute-api:{region}:{account_id}:{api_id}' \
                 '/*/GET/*/*'.format(region=region, account_id=account_id, api_id=gateway_api_id)

    try:
        return awslambda.add_permission(
            FunctionName=lambda_name,
            StatementId='apigateway-invoke-lambda',
            Action='lambda:InvokeFunction',
            Principal='apigateway.amazonaws.com',
            SourceArn=source_arn,
        )
    except awslambda.exceptions.ResourceConflictException as e:
        logging.info("Permission already added, not adding again")
        return awslambda.get_policy(FunctionName=lambda_name)


def delete_dnsdb_query_lambda(unique_id):
    function_name = 'lambda_dnsdb_query_py_{}'.format(unique_id)
    logging.info("Deleting Lambda function %s", function_name)
    awslambda = boto3.client('lambda')
    try:
        awslambda.delete_function(FunctionName=function_name)
    except awslambda.exceptions.ResourceNotFoundException as e:
        logging.info("Lambda function %s not found. Doing nothing", function_name)
