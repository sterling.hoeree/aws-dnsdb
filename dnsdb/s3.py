import logging
import boto3
import botocore


logging.basicConfig(level=logging.INFO)


def create_or_get_bucket(bucket_name, bucket_location='us-west-2'):
    s3 = boto3.resource('s3')
    try:
        s3.meta.client.head_bucket(Bucket=bucket_name)
    except botocore.exceptions.ClientError as e:
        error_code = int(e.response['Error']['Code'])
        if error_code == 403:
            raise Exception('Bucket {} exists but access is forbidden; choose another '
                            'bucket name to use'.format(bucket_name))
        elif error_code == 404:
            s3.Bucket(bucket_name).create(CreateBucketConfiguration={
                'LocationConstraint': bucket_location
            })


def recursive_delete(bucket, key_prefix):
    logging.info("Trying to delete from s3://%s/%s", bucket, key_prefix)
    s3 = boto3.resource('s3')
    objects_to_delete = s3.Bucket(bucket).objects.filter(Prefix=key_prefix)
    if objects_to_delete:
        logging.info("Deleting %s objects", len(list(objects_to_delete)))
        for x in objects_to_delete.delete():
            logging.info(str(x))


def single_upload(bucket_name, key, fp):
    s3 = boto3.client('s3')

    def progress(b):
        logging.info("s3://%s/%s Bytes uploaded: %s", bucket_name, key, b)

    s3.upload_fileobj(Fileobj=fp, Bucket=bucket_name, Key=key, Callback=progress)
