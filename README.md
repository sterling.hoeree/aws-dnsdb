# Serverless DNS DB application using AWS

## Prerequisites
Ensure that you have python2.7 and pip installed. Installing virtualenv (`pip install virtualenv`) 
and using a fresh one is recommended. After this is done, execute
```bash
pip install -r requirements.txt
```
to install all required libraries.

## Setup, execution, teardown

### Part one: configure AWS credentials
To set up the AWS environment, you will need to have an AWS account and
have configured your credentials on your machine.

To configure AWS credentials, use the `awscli` (if not installed install
with `pip install awscli`). Then, execute
```bash
aws configure
```

and enter your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and your `REGION`
(e.g. `us-west-2`).

Your account will need permissions to:
* Create Athena/Glue databases
* Upload and delete data to/from S3 buckets
* Create Lambda functions
* Create API Gateway REST APIs
* Create IAM roles

### Part two: the setup script
Set up your AWS environment by running the script `./create_aws_environment.py`.
You should pass in the `--data-file data-file.gz` (in my case, it was `authlogs.100k.gz`)
as well as the `--bucket bucket-name` you wish to store the data in. 

>Note on the data file. In a real-life scenario, the data file is likely not uploaded
>in this fashion, but files would likely make their way continuously into the destination
>bucket/prefix. Then, we could apply partitioning on the files 
>(e.g. `s3://bucket/ip=123.456.7.8/registered_domain=foo.com/data0000.gz`) which could
>then be reflected in the database (Athena/Glue) to possibly improve performance.

You can optionally provide an `--id unique-id` parameter which will be used to 
label functions and S3 prefixes for the environment, or you can let the script generate a 
random one for you. You can also reuse the same `--id` parameter if your script is 
interrupted partway through to continue setting up the same environment. 

An example for execution the setup script:
```bash
./create_aws_environment.py -f ~/Downloads/authlogs.100k.gz -b tech-element47-dev
```
Or, with a given ID:
```bash
./create_aws_environment.py --id 1234567890 -f ~/Downloads/authlogs.100k.gz -b tech-element47-dev
```

See `./create_aws_environment.py --help` if you need more assistance.

### Part three: using the REST API to query DNS data

When the script completes, you will see a message showing the public endpoint
for the REST API:

```
INFO:root:Finished setting up DNS DB AWS environment for 'DNS DB MSTGLE67'
INFO:root:Use this base url: https://s7n73tu5qh.execute-api.us-west-2.amazonaws.com/prod/
INFO:root:GET /ips_for_domain/{domain} - return the ips associated with a domain and its subdomains
INFO:root:GET /domains_for_ip/{ip} - return the domains for an ip
```

You can now use curl (for example) to get data from the endpoints:
```bash
% curl https://s7n73tu5qh.execute-api.us-west-2.amazonaws.com/prod/ips_for_domain/amazonaws.com | jq
```
```
{
  "ips": [
    "205.251.198.10",
    "204.13.250.31",
    "205.251.196.85",
    "205.251.196.163",
    "205.251.194.101",
    "205.251.194.187",
    "205.251.195.214", 
    ...],
   "domain": "amazonaws.com"
}
```

>Note: `jq` is used simply to pretty-print the json output.

The `domains_for_ip` endpoint functions similarly, but takes an ip instead.

### Part four: teardown

To destroy everything related to this REST API (i.e. Lambdas, S3 files, Athena databases, IAM roles, etc.)
simply execute the `./delete_aws_environment.py` command and give the `--id unique-id` 
and `--bucket bucket-name` (output is truncated for brevity):

```bash
% ./delete_aws_environment.py --id MSTGLE67 -b tech-element47-dev
```
```
INFO:root:Using bucket: tech-element47-dev
INFO:root:Deleting MSTGLE67/data
INFO:root:Trying to delete from s3://tech-element47-dev/MSTGLE67/data
...
INFO:root:Deleting 1 objects
...
INFO:root:Deleting Athena database dnsdb_MSTGLE67
...
INFO:root:Deleting REST API (s7n73tu5qh) DNS DB MSTGLE67
INFO:root:Deleting Lambda role LambdaBasicExecution_MSTGLE67
...
INFO:root:Detaching arn:aws:iam::aws:policy/AmazonS3FullAccess from LambdaBasicExecution_MSTGLE67
INFO:root:Detaching arn:aws:iam::aws:policy/AmazonAthenaFullAccess from LambdaBasicExecution_MSTGLE67
...
INFO:root:AWS environment for id MSTGLE67 was cleaned up
```

# Design considerations

## HTTP Server
Instead of making a container or VM, I opted to use AWS to spin up this application because of its
ability to scale well. I could have created a simple container running an HTTP REST server and containing
a database of some kind or linking to an exterbal database. However, if we wanted to scale by adding more
HTTP server nodes, we would then also have to set up a loadbalancer to abstract this away, and have some
way for the nodes to use the same database or share state with each other (if the database is embedded).

While AWS components can sometimes be challenging to configure, they do usually save money and resources.
Using a serverless architecture with API Gateway instead of an actual HTTP server on a node means that
we only pay for exactly what we use, and we don't have to maintain any actual machines.

## Database
For this demo, the database I used is Athena/Glue on top of files in S3. However, the files in S3 are not
stored into a partition, they are simply uploaded to a `data` prefix. Ideally, we would also include
information in the prefix such as `ip_address=123.456.7.8/registered_domain=foo.com` which could make
queries faster at the expense of more files and a more complicated architecture for continuously loading
the files. We would have also consider what would happen if we need to replace rows.

The database is also simply S3, not DynamoDB or another NoSQL or RDMS. I could have selected DynamoDB
instead, but for this simple application, it felt like overkill; DynamoDB could deal with the
problem mentioned earlier with row replacement, if we loaded each DNS query as a separate document and
provided proper unique keys, but at the expense of greater complexity and probably cost.

# Folder structure and resources
## dnsdb/
This directory contains almost all of the code used to provision the AWS environment
and tear it down. The only thing missing is the Athena code which is in the lambda function
in `resources/`.
>I opted not to use CloudFormation for such a small application. I believe that it is clunky
>and difficult to use, and is difficult to incrementally test. In a very large system though,
>perhaps it would be better to use CloudFormation templates and Stacks instead of boto3 and 
>python scripts to set up an AWS environment!

## resources/
* `lambda_basic_policy.yaml` - this file is the basic execution policy attached to the Lambda function
* `lambda_dnsdb_query.py` - this is the actual lambda function code that is used to query Athena/Glue
                            for the data stored in S3
* `swagger_rest_api.yaml` - a file describing the REST API, which is uploaded to API Gateway
* `template_create_table_dns_queries.sql` - a file describing the format of the authlogs.100k.gz file
                                            and is invoked to create the Athena/Glue table atop this data.
                                            The {location} is filled in by python.

