#!/usr/bin/env python2.7

import os
import argparse
import random
import string
import logging
import time
import boto3

from botocore.exceptions import ClientError

import dnsdb.s3
import dnsdb.sql
import dnsdb.functions
import dnsdb.iam
import dnsdb.gateway


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--id',
                        help='The id of the Lambda functions, bucket prefixes, etc. to delete',
                        required=True)
    parser.add_argument('-b', '--bucket-name',
                        help='Name of the app bucket being used, e.g. opendns-dev.',
                        required=True)

    return parser.parse_args()


def main():
    args = _parse_args()

    unique_id = args.id

    # Location of data
    s3_base_key_path = '{}/data'.format(unique_id)
    logging.info("Using bucket: %s", args.bucket_name)

    # Delete any data files under the path
    logging.info("Deleting %s", s3_base_key_path)
    dnsdb.s3.recursive_delete(bucket=args.bucket_name, key_prefix=s3_base_key_path)

    # Delete the Athena db and all tables
    db_name = 'dnsdb_{}'.format(unique_id)
    logging.info("Deleting Athena database %s", db_name)
    dnsdb.sql.drop_db(db_name=db_name)

    # Delete the Lambda function
    dnsdb.functions.delete_dnsdb_query_lambda(unique_id=unique_id)

    # Delete the API Gateway
    dnsdb.gateway.delete_api(unique_id=unique_id)

    # Delete the Lambda role
    lambda_role_name = 'LambdaBasicExecution_{}'.format(unique_id)
    logging.info("Deleting Lambda role %s", lambda_role_name)
    dnsdb.iam.delete_role(role_name=lambda_role_name)

    # Complete
    logging.info("AWS environment for id %s was cleaned up", unique_id)


if __name__ == '__main__':
    main()
