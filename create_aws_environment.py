#!/usr/bin/env python2.7

import os
import argparse
import random
import string
import logging
import time
import boto3

from botocore.exceptions import ClientError

import dnsdb.s3
import dnsdb.sql
import dnsdb.functions
import dnsdb.iam
import dnsdb.gateway


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--id',
                        help='Instead of generating a new ID, try to set up the environment using this ID instead. '
                             'Leave this argument empty if you are creating a new environment from scratch')
    parser.add_argument('-b', '--bucket-name',
                        help='Name of the app bucket to use or create, e.g. opendns-dev. '
                             'Data will be placed in s3://bucket-name/[Random Number]/data/',
                        required=True)
    parser.add_argument('-f', '--data-file',
                        help='Name of the .gz data file to upload to S3 now, if any')

    return parser.parse_args()


def main():
    args = _parse_args()

    if args.id:
        # Use a provided ID instead of creating a new one
        unique_id = args.id
    else:
        # ID to use for setting up this environment; it will become the suffix to database names, lambda names, etc.
        unique_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        logging.info("Created random id: %s", unique_id)

    # Location of data
    s3_base_key_path = '{}/data'.format(unique_id)

    # Use existing bucket or create new bucket
    dnsdb.s3.create_or_get_bucket(bucket_name=args.bucket_name)
    logging.info("Using bucket: %s", args.bucket_name)

    # Upload the data file, if given
    if args.data_file:
        file_key = os.path.join(s3_base_key_path, os.path.basename(args.data_file))
        logging.info("Will upload %s to %s", args.data_file, file_key)
        with open(args.data_file, 'rb') as fp:
            dnsdb.s3.single_upload(bucket_name=args.bucket_name, key=file_key, fp=fp)

    # Create the Athena table on top of the S3 file(s)
    # This won't create the table again if it already exists (see the .sql file)
    db_name = 'dnsdb_{}'.format(unique_id)
    logging.info("Creating Athena table over data in %s", s3_base_key_path)
    dnsdb.sql.create_dns_queries_table(bucket=args.bucket_name, data_key=s3_base_key_path, db_name=db_name)

    # Create the basic Lambda IAM role and attach the policies to invoke Athena
    lambda_role_name = 'LambdaBasicExecution_{}'.format(unique_id)
    lambda_role = dnsdb.iam.create_or_get_role(role_name=lambda_role_name,
                                               attach_managed_roles=['AmazonAthenaFullAccess', 'AmazonS3FullAccess'])

    # Create the lambda function which will be called to query data from S3
    ## XXX: Unfortunately, AWS will not update the role right away, so Lambda cannot access it until after 10-15
    ##      or so, so we have to busy wait.
    success = False
    last_message = ''
    for attempt in range(1, 15):
        try:
            lambda_info = \
                dnsdb.functions.create_or_update_dnsdb_query_lambda(unique_id=unique_id,
                                                                    bucket_name=args.bucket_name,
                                                                    db_name=db_name,
                                                                    lambda_role_arn=lambda_role['Role']['Arn'])
            success = True
            break
        except ClientError as e:
            if not e.response['Error']['Code'] == 'InvalidParameterValueException':
                raise e
            if last_message != e.message:
                last_message = e.message
                logging.error(e.message)

            logging.error("Will try again. Attempt #%s/15", attempt)
            time.sleep(1)

    if not success:
        raise e

    # Create the API Gateway which will have the two REST endpoints linked to the AWS Lambda function
    rest_api_info = dnsdb.gateway.create_or_get_api(unique_id=unique_id, lambda_arn=lambda_info['FunctionArn'])
    dnsdb.gateway.deploy_api(gateway_id=rest_api_info['id'], stage_name='prod')
    dnsdb.functions.add_gateway_access_to_lambda(lambda_name=lambda_info['FunctionName'],
                                                 gateway_api_id=rest_api_info['id'])

    # Complete
    api_url = 'https://{rest_api_id}.execute-api' \
              '.{region}.amazonaws.com/{stage_name}/'.format(rest_api_id=rest_api_info['id'],
                                                             region=boto3.client('apigateway').meta.region_name,
                                                             stage_name='prod')

    logging.info("Finished setting up DNS DB AWS environment for 'DNS DB %s'", unique_id)
    logging.info("Use this base url: %s", api_url)
    logging.info("GET /ips_for_domain/{domain} - return the ips associated with a domain and its subdomains")
    logging.info("GET /domains_for_ip/{ip} - return the domains for an ip")


if __name__ == '__main__':
    main()
