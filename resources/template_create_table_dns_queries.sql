CREATE EXTERNAL TABLE IF NOT EXISTS dns_queries (
  `ts` bigint,
  `domain_name` string,
  `record_type` string,
  `record_ttl` int,
  `return_value` int,
  `nameserver_ip` string,
  `registered_domain` string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES (
  'serialization.format' = '	',
  'field.delim' = '	'
) LOCATION '{location}'
TBLPROPERTIES ('has_encrypted_data'='false');
