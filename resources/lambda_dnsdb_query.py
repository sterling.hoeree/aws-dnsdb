import os
import boto3
import time
import logging
import json

logging.basicConfig(level=logging.INFO)


def str_trunc(s):
    trunc_s = s[:80].replace('\n', ' ')
    if len(trunc_s) < len(s):
        trunc_s += '...'
    return trunc_s


class Athena(object):
    def __init__(self, database, output_location=None):
        self._client = boto3.client('athena')
        self._db_config = {
            'Database': database.lower()
        }
        self._result_config = {
            'OutputLocation': output_location,
        } if output_location else {}

    def execute_and_wait(self, query_string):
        response = self._client.start_query_execution(
            QueryString=query_string,
            QueryExecutionContext=self._db_config,
            ResultConfiguration=self._result_config,
        )

        query_id = response['QueryExecutionId']
        logging.info("QueryExecutionId: %s: %s", query_id, str_trunc(query_string))

        while True:  # Could make this into a timeout wait instead for safety
            # Check query status by ID
            query_execution = self._client.get_query_execution(QueryExecutionId=query_id)
            query_status = query_execution['QueryExecution']['Status']['State']
            logging.info("Query %s status: %s", query_id, query_status)

            if query_status == 'SUCCEEDED':
                break

            if query_status == 'FAILED':
                raise Exception("Query {} FAILED. Details: {}".format(query_id, str(query_execution)))

            time.sleep(0.25)

        ## TODO: get all paginated results using qresult_dict['NextToken']
        return self._client.get_query_results(QueryExecutionId=query_id)


def build_select_query(table_name, ip=None, domain=None):
    """
    Builds either a SELECT DISTINCT nameserver_ip or a SELECT DISTINCT domain_name query
    depending on input parameters

    :param ip: If given, select the domains for this ip
    :param domain: If given, select the ips for this domain **and subdomains**
    :return: a SQL string
    :raises ValueError: if neither ip nor domain are given
    """
    if not (ip or domain):
        raise ValueError("Either ip or domain are required")

    if ip:
        return "SELECT DISTINCT domain_name FROM {} WHERE nameserver_ip = '{}'".format(table_name, ip)
    else:
        return "SELECT DISTINCT nameserver_ip FROM {} WHERE domain_name LIKE '%{}'".format(
            table_name, '{}.'.format(domain) if not domain.endswith('.') else domain)


def build_result_dict(result_list, ip_address=None, domain=None):
    if ip_address:
        body = {
            'ip': ip_address,
            'domains': result_list
        }
    else:
        body = {
            'domain': domain,
            'ips': result_list
        }

    return {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


def handler(event, context):
    # From the environment, which comes through in Lambda using the Environment dict
    s3_bucket = os.environ['S3_BUCKET']
    s3_athena_output_key_prefix = os.environ['S3_ATHENA_OUTPUT_KEY_PREFIX']
    athena_db = os.environ['ATHENA_DATABASE']
    athena_table = os.environ['ATHENA_TABLE']

    s3_athena_output_location = 's3://{}/{}/'.format(s3_bucket, s3_athena_output_key_prefix)

    # Connect to Athena, which queries data directly off of S3
    athena = Athena(database=athena_db, output_location=s3_athena_output_location)

    ip_query = 'ip' in event['pathParameters']
    ip_address = None
    domain = None
    if ip_query:
        ip_address = event['pathParameters']['ip']
        query = build_select_query(table_name=athena_table, ip=ip_address)

    elif 'domain' in event['pathParameters']:
        domain = event['pathParameters']['domain']
        query = build_select_query(table_name=athena_table, domain=domain)

    else:
        raise ValueError("Path must contain either /{ip}/ or /{domain}/")

    # Fetch results
    logging.info("Executing Athena query: %s", query)
    qresult_dict = athena.execute_and_wait(query_string=query)

    result_list = []
    rows = qresult_dict['ResultSet']['Rows']
    result_list.extend([r['Data'][0]['VarCharValue'] for r in rows[1:]])
    logging.info("Returning %s %s rows", len(result_list), 'IP' if ip_query else 'Domain')

    return build_result_dict(ip_address=ip_address, domain=domain, result_list=result_list)
